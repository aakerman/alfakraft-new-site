/**
	* @title: Google Chart - Responsive - Alfakraft.se - All Charts
	* @created:	2015-06-01
	* @author: 	Robert O. Aakerman
	* @http:	aakerman.net
	* @email:	robert@aakerman.net
*/

//Load Google Visualization Line Chart
google.load('visualization', '1', {packages: ['corechart', 'line']});
google.setOnLoadCallback(drawChart);

	function drawChart() {	//Set all variables for each chart. Selecting the columns in each Google Sheet
      var queryStringAQFFront = encodeURIComponent('SELECT A, B, C, D, E'); 
      var queryStringACF = encodeURIComponent('SELECT A, B'); 
      var queryStringAEF = encodeURIComponent('SELECT A, B'); 
      var queryStringASF = encodeURIComponent('SELECT A, B'); 
      var queryStringARF = encodeURIComponent('SELECT A, B');	
      var queryStringAQF2xL = encodeURIComponent('SELECT A, B');
      var queryStringAQF = encodeURIComponent('SELECT A, B');	  

		//Call all Google Sheets and add the Query string
		var aqffront = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1YaEbmyIjwLNn7pRNxBgEG8kLLSstn1hQe259zkvu66s/gviz/tq?sheet=aqf-front-chart&headers=1&tq=' + queryStringAQFFront);
		var acf = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1LkPmo3ZwEwsNhoqsi-dD0ZbjKkeMowXbVj9c31oDqbg/gviz/tq?sheet=acf-chart&headers=1&tq=' + queryStringACF);    
		var aef = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1M9f4CXYe_0NSu6mMWqwuM5FDZGvUOsDd_sK6EY066TI/gviz/tq?sheet=aef-chart&headers=1&tq=' + queryStringAEF);  
		var asf = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1xC68rPze-0jYAhnyH-tOMocy1UsBnfravWdgebIOrx8/gviz/tq?sheet=asf-chart&headers=1&tq=' + queryStringASF);  
		var arf = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1xvqYP5zIzIVo_FJszYYSh0itDu-UOW44XKo0qeTNyZQ/gviz/tq?sheet=arf-chart&headers=1&tq=' + queryStringARF); 
		var aqf2xl = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1BT43vaw6dPPMSxX6EyDdO7nFh_6eVPmF117G9NwhCuU/gviz/tq?sheet=aqf2xl-chart&headers=1&tq=' + queryStringAQF2xL); 	
		var aqf = new google.visualization.Query(
          'https://docs.google.com/spreadsheets/d/1YocFODZXmqM8dLhP4dQNW6AnW8MkmVwppn8R9rwbyyU/gviz/tq?sheet=aqf-chart&headers=1&tq=' + queryStringAQF2xL); 		  
		  
		  	//Send all requests and get response
			aqffront.send(handleQueryResponseAQFFront);
			acf.send(handleQueryResponseACF);
			aef.send(handleQueryResponseAEF);
			asf.send(handleQueryResponseASF);
			arf.send(handleQueryResponseARF);	
			aqf2xl.send(handleQueryResponseAQF2xL);	
			aqf.send(handleQueryResponseAQF);				
	}

		//Alfa Quantitative Frontpage Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseAQFFront(response) {
			  
			var element =  document.getElementById('aqf_front_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {				  

			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Quant Fund', titleTextStyle: {color: '#666', fontSize: '30', bold: false},
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				series: {
					0: { color: '#45818E' },
					1: { color: '#4A86E8' },
					2: { color: '#6AA84F' },
					3: { color: '#FF0000' },
						  },		  
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartAQFF = new google.visualization.LineChart(document.getElementById('aqf_front_chart_div'));
				  chartAQFF.draw(data, options, { height: 400 });
				  
				function resize () {
						chartAQFF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}
		}	

		//Alfa Commodity Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseACF(response) {

			var element =  document.getElementById('acf_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		  
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Commodity Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartACF = new google.visualization.LineChart(document.getElementById('acf_chart_div'));
				  chartACF.draw(data, options, { height: 400 });
				  
				function resize () {
						chartACF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}
		}  

		//Alfa Edge Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseAEF(response) {

			var element =  document.getElementById('aef_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		
		
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Edge Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartAEF = new google.visualization.LineChart(document.getElementById('aef_chart_div'));
				  chartAEF.draw(data, options, { height: 400 });

				function resize () {
						chartAEF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}
		}

		//Alfa Saga Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseASF(response) {

			var element =  document.getElementById('asf_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Saga Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartASF = new google.visualization.LineChart(document.getElementById('asf_chart_div'));
				  chartASF.draw(data, options, { height: 400 });

				function resize () {
						chartASF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}  
		}

		//Alfa Rubicon Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseARF(response) {

			var element =  document.getElementById('arf_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Rubicon Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartARF = new google.visualization.LineChart(document.getElementById('arf_chart_div'));
				  chartARF.draw(data, options, { height: 400 });

				function resize () {
						chartARF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}  
		}

		//Alfa Quantitative 2xL Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseAQF2xL(response) {

			var element =  document.getElementById('aqf2xl_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Quant 2xL Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:98 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartAQF2xL = new google.visualization.LineChart(document.getElementById('aqf2xl_chart_div'));
				  chartAQF2xL.draw(data, options, { height: 400 });

				function resize () {
						chartAQF2xL.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}   
		}

		//Alfa Quantitative Chart response. Find by id, skip or present error. If found add options, draw chart and redraw if window changes
		function handleQueryResponseAQF(response) {

			var element =  document.getElementById('aqf_chart_div');
			if (typeof(element) != 'undefined' && element != null)  {		  
		
			  if (response.isError()) {
				alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
				return;
			  }
				var options = {
				  title: 'Alfa Quant Fund',
				  hAxis: {title: 'Per Month', titleTextStyle: {color: '#333'}},	
				  vAxis: {viewWindowMode:'explicit', viewWindow: { min:100 }, gridlines: {count: '-1'}, minValue: '100'},
				  legend: { position: 'bottom' }
				};

				  var data = response.getDataTable();
				  var chartAQF = new google.visualization.LineChart(document.getElementById('aqf_chart_div'));
				  chartAQF.draw(data, options, { height: 400 });

				function resize () {
						chartAQF.draw(data, options, { height: 400 });			
				}
				if (window.addEventListener) {
					window.addEventListener('resize', resize);
				}
				else {
					window.attachEvent('onresize', resize);
				} 		  
			}    
		}	
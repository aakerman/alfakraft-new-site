# README #

JavaScript for Responsive Google Charts.
Made for http://Alfakraft.se in 2015

### What is this repository for? ###

* By making one comprehensive JavaScript file for all Charts on site and using Google Sheets as data input the client can easily update the charts without backend access. 
* The requirement was also to make the charts responsive.
* Version 1.0

### Live example at: ###
https://jsbin.com/wimudas/edit?html,css,js,output
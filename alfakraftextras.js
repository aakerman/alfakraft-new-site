/*
@title: Extra snippets for Alfakraft.se
@author: Robert O. Aakerman
@email: info@aakerman.net
@date: 2015-11-12
*/

// Adding Classes to Frontpage login button 
window.onload = function() {
	var parentId = document.getElementById('menu-item-1092');
	
 parentId.className += " vc_general vc_btn3 vc_btn3-size-lg vc_btn3-shape-square vc_btn3-style-modern vc_btn3-color-blue";
 parentId.getElementsByTagName('a')[0].style.color = "#FFFFFF";
 };